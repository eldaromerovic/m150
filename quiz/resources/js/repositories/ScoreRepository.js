import Repository from 'axios';

const resource = '/score';

export default {
    get() {
        return Repository.get(`${resource}`);
    },

    addScore(pointValue) {
        return Repository.post(`${resource}`, { points: pointValue } );
    },
}