<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ScoreController extends Controller
{
    
    public function indexForScore()
    {
        $users = \DB::table('users')
            ->orderBy('points', 'desc')
            ->get();

        return $users;
    }
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function store(Request $request)
    {
        $request->validate([
            'points' => 'required|numeric'
        ]);
        
        $points = $request->get('points');
        $this->addScore($points);
    }

    public function addScore($amount)
    {
        $user = Auth::user();
        //return response()->json(['success' => isset($user) ? $user : 'php isch scheisse'], 200);
        $userId = $user->id;

        $score = \DB::table('users')
            ->select('points')
            ->where('id', $userId)
            ->get();

        \DB::table('users')
            ->where('id', $userId)
            ->update(['points' => $score[0]->points + $amount]);
    }
}
